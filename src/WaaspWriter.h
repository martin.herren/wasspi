#pragma once

#include <atomic>
#include <string>
//#include <thread>

#include <QObject>
#include <QThread>
#include <QSerialPort>

class WaaspWriter : public QThread {
    Q_OBJECT

public:
    explicit WaaspWriter(std::string const & serialPortName = "", int baudrate = 115200, int rate = 0);
    ~WaaspWriter();

    void stop();

public slots:
    void setAttitude(float roll, float pitch);

private:
    void run() override;
    void _sendFrame();

    std::string _serialPortName;
    int _baudrate;
    int _rate;

    float _roll;
    float _pitch;

    std::atomic_bool _run;
    std::thread _thread;

    QSerialPort _serialPort;
};
