#include "WaaspWriter.h"

#include <chrono>
#include <cmath>
#include <cstdlib>
#include <functional>
#include <iomanip>
#include <iostream>
#include <sstream>

#include "Tss1Formater.h"

WaaspWriter::WaaspWriter(std::string const & serialPortName, int baudrate, int rate) :
_serialPortName{serialPortName},
_baudrate{baudrate},
_rate{rate},
_roll{0},
_pitch{0},
_run{false} {
    _serialPort.setPortName(QString::fromStdString(_serialPortName));
    _serialPort.setBaudRate(_baudrate);

    if (!_serialPort.open(QIODevice::WriteOnly)) {
        throw std::runtime_error("WaaspWriter: Failed to open port, error: " + _serialPort.errorString().toStdString());
    }
    _sendFrame();

    if (_rate > 0) {
        _run = true;
//        _thread = std::thread(std::bind(&WaaspWriter::_loop, this));
        start();
    }
}

WaaspWriter::~WaaspWriter() {
    if (_run) {
        _run = false;
        _thread.join();
    }

    _serialPort.close();
}

void WaaspWriter::stop() {
    _run = false;
}

void WaaspWriter::setAttitude(float roll, float pitch) {
    _roll = roll;
    _pitch = pitch;

    if (_rate == 0) {
        // Loop doesn't run if _rate is 0, so send now
        _sendFrame();
    }
}

void WaaspWriter::run() {
    int sleepInMs = 1000 / _rate;

    while (_run) {
        auto nextFrame = std::chrono::steady_clock::now() + std::chrono::milliseconds(sleepInMs);
        _sendFrame();
        std::this_thread::sleep_until(nextFrame);
    }
}

void WaaspWriter::_sendFrame() {
    auto frame{Tss1Formater::formatFrame(_roll, _pitch)};
//    std::cout << Tss1Formater::formatFrame(_roll, _pitch) << std::endl;
    _serialPort.write(
        QByteArray{frame.c_str(), static_cast<int>(frame.length())}
    );
}
