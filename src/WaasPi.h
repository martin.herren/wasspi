#pragma once

#include <memory>
#include <string>

#include <QObject>

#include "MavlinkReader.h"
#include "WaaspWriter.h"

class WaasPi : public QObject {
    Q_OBJECT

public:
    WaasPi(
        std::string const & mavlinkDevice,
        std::string const & waaspDevice
    );
    void start();

private:
    std::unique_ptr<MavLinkReader> _mavlinkReader;
    std::unique_ptr<WaaspWriter> _waaspWriter;
};
