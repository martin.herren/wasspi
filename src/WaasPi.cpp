#include "WaasPi.h"

WaasPi::WaasPi(
    std::string const & mavlinkDevice,
    std::string const & waaspDevice
) :
_mavlinkReader{new MavLinkReader(mavlinkDevice, 115200, 1)},
_waaspWriter{new WaaspWriter(waaspDevice, 115200, 0)} {
    connect(
        _mavlinkReader.get(), &MavLinkReader::attitudeChanged,
        _waaspWriter.get(), &WaaspWriter::setAttitude
    );
    start();
}

void WaasPi::start() {
    _mavlinkReader->start();
}
