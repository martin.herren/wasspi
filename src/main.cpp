#include <iostream>

#include <clara.hpp>
#include <yaml-cpp/yaml.h>

#include <QCoreApplication>

#include "WaasPi.h"

int main(int argc, char * argv[]) {
    QCoreApplication app(argc, argv);

    bool help{false};
    std::string config_file{};
    std::string device_mavlink{};
    std::string device_tss1{};

    using namespace clara;

    auto cli =
    Opt(config_file, "configfile" )["-c"]["--configfile"]("config file")
        | Opt(device_mavlink, "device" )["-m"]["--mavlinkdevice"]("MavLink device")
        | Opt(device_tss1, "device" )["-t"]["--tss1device"]("TSS1 devic")
        | Help(help);

    auto result = cli.parse(Args(argc, argv));

    if (!result) {
        std::cerr << "Error in command line: " << result.errorMessage() << std::endl;
        std::cout << cli;
        exit(1);
    }

    if (help) {
        std::cout << cli;
        exit(0);
    }

    if (config_file.length() > 0) {
        YAML::Node config = YAML::LoadFile(config_file);

        if (config["devices"]) {
            if (!config["devices"].IsMap()) {
        		throw std::runtime_error("YAML: devices must be a map");
        	}

            // Override devices if not already set
            if (device_mavlink.length() == 0 && config["devices"]["mavlink"]) {
                device_mavlink = config["devices"]["mavlink"].as<std::string>();
            }
            if (device_tss1.length() == 0 && config["devices"]["tss1"]) {
                device_tss1 = config["devices"]["tss1"].as<std::string>();
            }
        }
    }

    if (device_mavlink.length() == 0 || device_tss1.length() == 0) {
        std::cerr << "Mavlink and TSS1 devices must be given in config file or command line" << std::endl;
        exit(-1);
    }

    WaasPi imu{device_mavlink, device_tss1};

    app.exec();

    return 0;
}
