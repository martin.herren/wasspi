#include "MavlinkReader.h"

#include <array>
#include <chrono>
#include <cmath>
#include <cstdlib>
#include <exception>
#include <functional>
#include <future>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <thread>

#include <QSerialPort>

MavLinkReader::MavLinkReader(std::string const & serialPortName, int baudrate, int rate) :
_serialPortName{serialPortName},
_baudrate{baudrate},
_rate{rate},
_run{true} {
}

MavLinkReader::~MavLinkReader() {
    if (_run) {
        stop();
        wait();
    }
}

void MavLinkReader::stop() {
    _run = false;
}

void MavLinkReader::run() {
    if (_serialPortName.compare("") == 0) {
        quit();
        return;
    }

    QSerialPort serialPort;
    serialPort.setPortName(QString::fromStdString(_serialPortName));
    serialPort.setBaudRate(_baudrate);

    if (!serialPort.open(QIODevice::ReadWrite)) {
        std::cout << QObject::tr("MavLinkReader: Failed to open port, error: %1").arg(serialPort.errorString()).toStdString() << std::endl;
        quit();
        return;
    }

    std::cout << "Running " << std::endl;

    std::array<unsigned char, MAVLINK_MAX_PACKET_LEN> sendBuffer;
    int sendBufferSize = _formatSetRate(sendBuffer, MAV_DATA_STREAM_ALL, 10);
    serialPort.write(reinterpret_cast<char *>(&(std::get<0>(sendBuffer))), sendBufferSize);

    _run = true;

    while (_run) {
        if (serialPort.waitForReadyRead(-1)) {
            std::array<char, 1024> array;
            int available = serialPort.bytesAvailable();
            if (available > array.size()) available = array.size();
            available = serialPort.read(&(std::get<0>(array)), available);

            std::for_each(
                std::begin(array),
                std::begin(array) + available,
                [this](char const & c) {
                    _process(c);
                }
            );
        }
    }

    std::cout << "Closing " << std::endl;

    serialPort.close();

    quit();
}

int MavLinkReader::_formatSetRate(std::array<unsigned char, MAVLINK_MAX_PACKET_LEN> & sendBuffer, int stream, int rate) {
    mavlink_message_t msg;

    mavlink_msg_data_stream_pack(255, MAV_COMP_ID_UART_BRIDGE, &msg, stream, rate, (rate == 0 ? 0 : 1));

    return mavlink_msg_to_send_buffer(static_cast<unsigned char *>(&(std::get<0>(sendBuffer))), &msg);
}

void MavLinkReader::_process(char c) {
    int chan = MAVLINK_COMM_0;
    mavlink_status_t status;
    mavlink_message_t msg;

    if (mavlink_parse_char(chan, c, &msg, &status)) {
        switch(msg.msgid) {
            case MAVLINK_MSG_ID_HEARTBEAT: // 0
            {
                mavlink_heartbeat_t h;
                mavlink_msg_heartbeat_decode(&msg, &h);
                std::cout << "\nHeartbeat\tversion: " << (int)h.mavlink_version << std::endl;
            }
            break;

            case MAVLINK_MSG_ID_ATTITUDE: // 30
            {
                mavlink_attitude_t a;
                mavlink_msg_attitude_decode(&msg, &a);
                std::cout << "\nAttitude\ttime_boot_ms: " << a.time_boot_ms << " roll: " << a.roll << " pitch: " << a.pitch << " yaw: " << a.yaw << " rollspeed: " << a.rollspeed << " pitchspeed: " << a.pitchspeed << " yawspeed: " << a.yawspeed << std::endl;
            }
            break;

            case MAVLINK_MSG_ID_GLOBAL_POSITION_INT: // 33
            {
                mavlink_global_position_int_t p;
                mavlink_msg_global_position_int_decode(&msg, &p);
                std::cout << "\nGlobal position\ttime_boot_ms: " << p.time_boot_ms << " lat: " << p.lat << " lon: " << p.lon << " alt: " << p.alt << " relative_alt: " << p.relative_alt << " vx: " << p.vx << " vy: " << p.vy  << " vz: " << p.vz << " hdg: " << p.hdg << std::endl;
            }
            break;

            case MAVLINK_MSG_ID_TIMESYNC: // 110
            {
                mavlink_timesync_t t;
                mavlink_msg_timesync_decode(&msg, &t);
                std::cout << "\nTimesync\ttc1: " << t.tc1 << "ts1: " << t.ts1 << std::endl;
            }
            break;

            case MAVLINK_MSG_ID_ALTITUDE: // 141
            {
                mavlink_altitude_t a;
                mavlink_msg_altitude_decode(&msg, &a);
                std::cout << "\nAltitude\ttime_usec: " << a.time_usec << " mono: " << a.altitude_monotonic << " amsl: " << a.altitude_amsl << " loc: " << a.altitude_local << " rel: " << a.altitude_relative << " terr: " << a.altitude_terrain << " clear: " << a.bottom_clearance << std::endl;
            }
            break;

            case MAVLINK_MSG_ID_AHRS2: // 178
            {
                mavlink_ahrs2_t a;
                mavlink_msg_ahrs2_decode(&msg, &a);
                std::cout << "\nAHRS2\troll: " << a.roll << " pitch: " << a.pitch << " yaw: " << a.yaw << " altitude: " << a.altitude << " lat: " << a.lat << " lng: " << a.lng << std::endl;
                emit attitudeChanged(180 * a.roll / M_PI, 180 * a.pitch / M_PI);
            }
            break;

            case MAVLINK_MSG_ID_AHRS3: // 182
            {
                mavlink_ahrs3_t a;
                mavlink_msg_ahrs3_decode(&msg, &a);
                std::cout << "\nAHRS3\troll: " << a.roll << " pitch: " << a.pitch << " yaw: " << a.yaw << " altitude: " << a.altitude << " lat: " << a.lat << " lng: " << a.lng << std::endl;
            }
            break;

            default:
                std::cout << "\nUnknown message received with ID " << (int)msg.msgid << ", sequence: " << (int)msg.seq << " from component " << (int)msg.compid << " of system " << (int)msg.sysid << std::endl;
                break;
        }
    }
}
