#include "catch.hpp"

#include "Tss1Formater.h"

TEST_CASE("Tss1Formater.formatFrame", "[tss1]") {
    // Test 0
    CHECK(Tss1Formater::formatFrame(0, 0) == ":000000  0000U 0000  0000\r\n");
    CHECK(Tss1Formater::formatFrame(-0, -0) == ":000000  0000U 0000  0000\r\n");

    // Test roll
    CHECK(Tss1Formater::formatFrame(-100.000, 0) == ":000000  0000U-9999  0000\r\n");
    CHECK(Tss1Formater::formatFrame( -99.999, 0) == ":000000  0000U-9999  0000\r\n");
    CHECK(Tss1Formater::formatFrame( -99.990, 0) == ":000000  0000U-9999  0000\r\n");
    CHECK(Tss1Formater::formatFrame( -90.000, 0) == ":000000  0000U-9000  0000\r\n");
    CHECK(Tss1Formater::formatFrame( -10.000, 0) == ":000000  0000U-1000  0000\r\n");
    CHECK(Tss1Formater::formatFrame(  -1.000, 0) == ":000000  0000U-0100  0000\r\n");
    CHECK(Tss1Formater::formatFrame(  -0.100, 0) == ":000000  0000U-0010  0000\r\n");
    CHECK(Tss1Formater::formatFrame(  -0.010, 0) == ":000000  0000U-0001  0000\r\n");
    CHECK(Tss1Formater::formatFrame(  -0.001, 0) == ":000000  0000U-0000  0000\r\n");

    CHECK(Tss1Formater::formatFrame(   0.001, 0) == ":000000  0000U 0000  0000\r\n");
    CHECK(Tss1Formater::formatFrame(   0.010, 0) == ":000000  0000U 0001  0000\r\n");
    CHECK(Tss1Formater::formatFrame(   0.100, 0) == ":000000  0000U 0010  0000\r\n");
    CHECK(Tss1Formater::formatFrame(   1.000, 0) == ":000000  0000U 0100  0000\r\n");
    CHECK(Tss1Formater::formatFrame(  10.000, 0) == ":000000  0000U 1000  0000\r\n");
    CHECK(Tss1Formater::formatFrame(  90.000, 0) == ":000000  0000U 9000  0000\r\n");
    CHECK(Tss1Formater::formatFrame(  99.990, 0) == ":000000  0000U 9999  0000\r\n");
    CHECK(Tss1Formater::formatFrame(  99.999, 0) == ":000000  0000U 9999  0000\r\n");
    CHECK(Tss1Formater::formatFrame( 100.000, 0) == ":000000  0000U 9999  0000\r\n");

    // Test pitch
    CHECK(Tss1Formater::formatFrame(0, -100.000) == ":000000  0000U 0000 -9999\r\n");
    CHECK(Tss1Formater::formatFrame(0,  -99.999) == ":000000  0000U 0000 -9999\r\n");
    CHECK(Tss1Formater::formatFrame(0,  -99.990) == ":000000  0000U 0000 -9999\r\n");
    CHECK(Tss1Formater::formatFrame(0,  -90.000) == ":000000  0000U 0000 -9000\r\n");
    CHECK(Tss1Formater::formatFrame(0,  -10.000) == ":000000  0000U 0000 -1000\r\n");
    CHECK(Tss1Formater::formatFrame(0,   -1.000) == ":000000  0000U 0000 -0100\r\n");
    CHECK(Tss1Formater::formatFrame(0,   -0.100) == ":000000  0000U 0000 -0010\r\n");
    CHECK(Tss1Formater::formatFrame(0,   -0.010) == ":000000  0000U 0000 -0001\r\n");
    CHECK(Tss1Formater::formatFrame(0,   -0.001) == ":000000  0000U 0000 -0000\r\n");

    CHECK(Tss1Formater::formatFrame(0,    0.001) == ":000000  0000U 0000  0000\r\n");
    CHECK(Tss1Formater::formatFrame(0,    0.010) == ":000000  0000U 0000  0001\r\n");
    CHECK(Tss1Formater::formatFrame(0,    0.100) == ":000000  0000U 0000  0010\r\n");
    CHECK(Tss1Formater::formatFrame(0,    1.000) == ":000000  0000U 0000  0100\r\n");
    CHECK(Tss1Formater::formatFrame(0,   10.000) == ":000000  0000U 0000  1000\r\n");
    CHECK(Tss1Formater::formatFrame(0,   90.000) == ":000000  0000U 0000  9000\r\n");
    CHECK(Tss1Formater::formatFrame(0,   99.990) == ":000000  0000U 0000  9999\r\n");
    CHECK(Tss1Formater::formatFrame(0,   99.999) == ":000000  0000U 0000  9999\r\n");
    CHECK(Tss1Formater::formatFrame(0,  100.000) == ":000000  0000U 0000  9999\r\n");
}
