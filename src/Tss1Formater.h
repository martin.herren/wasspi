#pragma once

#include <string>

class Tss1Formater {
public:
    // roll and pitch in degrees
    static std::string formatFrame(float roll, float pitch);
};
