#include "Tss1Formater.h"

#include <cmath>
#include <iomanip>
#include <sstream>

std::string Tss1Formater::formatFrame(float roll, float pitch) {
    std::stringstream ss;

    // Truncate values to allowed range
    if (roll < -99.99) roll = -99.99;
    if (roll > 99.99) roll = 99.99;
    if (pitch < -99.99) pitch = -99.99;
    if (pitch > 99.99) pitch = 99.99;

    short int hAcceleration = 0;
    short int vAcceleration = 0;
    short int heave = 0;

    ss << std::setfill('0') << std::internal;
    ss << ":";
    ss << std::setw(2) << hAcceleration;
    ss << std::setw(4) << vAcceleration;
    ss << " ";
    ss << (heave < 0 ? "-" : " ");
    ss << std::setw(4) << std::abs(heave);
    ss << "U"; // Status flag: unaided
    ss << (roll < 0 ? "-" : " ");
    ss << std::setw(4) << std::floor(std::abs(roll) * 100);
    ss << " ";
    ss << (pitch < 0 ? "-" : " ");
    ss << std::setw(4) << std::floor(std::abs(pitch) * 100);
    ss << "\r\n";

    return ss.str();
}
