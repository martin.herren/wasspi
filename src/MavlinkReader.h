#pragma once

#include <atomic>
#include <string>

#include <QObject>
#include <QThread>

#include "ardupilotmega/mavlink.h"

class MavLinkReader : public QThread {
    Q_OBJECT

public:
    explicit MavLinkReader(std::string const & serialPortName = "", int baudrate = 115200, int rate = 0);
    ~MavLinkReader();

    void stop();

signals:
    void attitudeChanged(float roll, float pitch);

private:
    void run() override;
    int _formatSetRate(std::array<unsigned char, MAVLINK_MAX_PACKET_LEN> & sendBuffer, int stream, int rate);
    void _process(char c);

    std::string _serialPortName;
    int _baudrate;
    int _rate;

    std::atomic_bool _run;
};
