[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0)
[![Build Status](https://gitlab.com/hb9fxx/qrsspig/badges/dev/build.svg)](https://gitlab.com/hb9fxx/qrsspig/commits/dev)

# WaasPi
Simple MavLink to TSS1 gateway for Raspberry Pi Zero for a Waasp.

With WaasPi you can connect a ArduPilot flight controller's telemetry output as an IMU input for a [WASSP](https://wassp.com/) multibeam sonar to compensate a boat's roll and pitch. Can probably work with other brand of sonars accepting TSS1 input.
